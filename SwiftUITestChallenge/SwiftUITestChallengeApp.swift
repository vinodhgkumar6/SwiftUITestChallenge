//
//  SwiftUITestChallengeApp.swift
//  SwiftUITestChallenge
//
//  Created by vinodh kumar on 06/07/22.
//

import SwiftUI

@available(iOS 14.0, *)
struct SwiftUITestChallengeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

@main
struct WeatherProAppWrapper {
    static func main() {
        if #available(iOS 14.0, *) {
            SwiftUITestChallengeApp.main()
        }
        else {
            UIApplicationMain(CommandLine.argc, CommandLine.unsafeArgv, nil, NSStringFromClass(SceneDelegate.self))
        }
    }
}
