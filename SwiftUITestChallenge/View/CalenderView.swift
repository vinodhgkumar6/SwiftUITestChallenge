//
//  CalenderView.swift
//  Calendar2
//
//  Created by vino on 06/07/22..
//

import Foundation
import SwiftUI

struct WeekView<DateView>: View where DateView: View {

    let week: Date
    let content: (Date) -> DateView
    var isHideDaysNotInMonth = true
    let calendar = Calendar(identifier: .iso8601)
    
    init(week: Date, isHideDaysNotInMonth: Bool = true, @ViewBuilder content: @escaping (Date) -> DateView) {
        self.week = week
        self.isHideDaysNotInMonth = isHideDaysNotInMonth
        self.content = content
    }

    private var days: [Date] {
        guard
            let weekInterval = calendar.dateInterval(of: .weekOfYear, for: week)
            else { return [] }
        let days = calendar.generateDates(
            inside: weekInterval,
            matching: DateComponents(hour: 0, minute: 0, second: 0)
        )
        return days
    }

    var body: some View {
        HStack {
            ForEach(days, id: \.self) { date in
                HStack {
                    if isHideDaysNotInMonth {
                        if self.calendar.isDate(self.week, equalTo: date, toGranularity: .month) {
                            self.content(date)
                        } else {
                            self.content(date)
                                .foregroundColor(.gray)
                                .disabled(true)
                        }
                    } else {
                        self.content(date)
                    }
                }
                .frame(maxWidth: .infinity)
            }
        }
    }
}


struct MonthView<DateView>: View where DateView: View {

    let month: Date
    let content: (Date) -> DateView
    let calendar = Calendar(identifier: .iso8601)
    
    init(
        month: Date,
        @ViewBuilder content: @escaping (Date) -> DateView
    ) {
        self.month = month
        self.content = content
    }

    private var weeks: [Date] {
        guard
            let monthInterval = calendar.dateInterval(of: .month, for: month)
            else { return [] }
        let days = calendar.generateDates(
            inside: monthInterval,
            matching: DateComponents(hour: 0, minute: 0, second: 0, weekday: calendar.firstWeekday))
        return days
    }

    var body: some View {
        VStack(spacing: 4) {
            ForEach(weeks, id: \.self) { week in
                WeekView(week: week, content: self.content)
            }
        }
    }
    
}
