//
//  ContentView.swift
//  SwiftUITestChallenge
//
//  Created by vinodh kumar on 06/07/22.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject var viewModel: CalendarViewModel = CalendarViewModel()
    
    var body: some View {
        VStack(spacing: 12) {
            HStack{
                Spacer()
                Button(action: {
                    
                }, label: {
                    Text("Test")
                })
            }
            Text("SwiftUI Test Challenge")
                .frame(maxWidth: .infinity)
                .foregroundColor(Color.label)
                .font(.largeTitle)
            
            SingleLineCalendarView(viewModel: viewModel)
                .frame(height: 75)
            Spacer()
        }
        .padding()
        .compatibleFullScreen(isPresented: $viewModel.showPopupCalendar) {
            ZStack{
                Color.clear.edgesIgnoringSafeArea(.all)
                VStack {
                    Spacer(minLength: 20)
                    ContainerView(viewModel: viewModel)
                    Spacer(minLength: 20)
                }
            }
            .background(TransparentBackground())
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct TransparentBackground: UIViewRepresentable {

    func makeUIView(context: Context) -> UIView {
        let view = UIView()
        DispatchQueue.main.async {
            view.superview?.superview?.backgroundColor = UIColor.white.withAlphaComponent(0.75)
        }
        return view
    }

    func updateUIView(_ uiView: UIView, context: Context) {}
}
