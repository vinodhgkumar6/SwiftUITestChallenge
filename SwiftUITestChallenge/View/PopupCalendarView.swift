//
//  PopupCalendarView.swift
//  SwiftUITestChallenge
//
//  Created by vinodh kumar on 06/07/22.
//

import SwiftUI

struct HeaderView: View {
    
    @Binding var currentMonth: Int
    @Binding var selectedDate: Date
    @ObservedObject var viewModel: CalendarViewModel
    
    private var header: some View {
        let formatter = DateFormatter.monthAndYear
        return Text(formatter.string(from: Calendar.current.date(byAdding: .month, value: currentMonth, to: Date())!))
            .foregroundColor(.red)
            .font(.subheadline)
            .fontWeight(.bold)
            .padding()
    }
    
    var body: some View {
        HStack {
            Button(action: {
                selectedDate = Calendar.current.startOfDay(for: Date())
                viewModel.setCurrentDateAsToday()
            }, label: {
                Text("Today")
                    .font(.subheadline)
                    .foregroundColor(.white)
                    .padding(4)
                    .background(Capsule()
                        .fill(.red))
            })
            Spacer()
            Button(action: {
                withAnimation{
                    viewModel.updatePopCalenderView(isPrevious: true)
                }
            }, label: {
                Image(systemName: "chevron.left")
                    .foregroundColor(.red)
            })
            
            header
            
            Button(action: {
                withAnimation{
                    viewModel.updatePopCalenderView(isPrevious: false)
                }
            }, label: {
                Image(systemName: "chevron.right")
                    .foregroundColor(.red)
            })
            Spacer()
            Button(action: {
                //                self.viewModel.fetchData()
                viewModel.showPopupCalendar = false
            }, label: {
                Image(systemName: "xmark.circle.fill")
                    .imageScale(.large)
                    .foregroundColor(.red)
            })
            
        }
        .padding(.horizontal, 8)
    }
    
}

struct ContainerView: View {
    
    @ObservedObject var viewModel: CalendarViewModel
    
    var body: some View {
        ZStack {
            
            PopupCalendarView(viewModel: viewModel)
                .background(Color.systemBackground)
            
            HStack {
                Rectangle()
                    .fill(.white)
                    .frame(width: 16)
                    .frame(maxHeight: .infinity)
                Spacer()
            }
            
            HStack {
                Spacer()
                Rectangle()
                    .fill(.white)
                    .frame(width: 16)
                    .frame(maxHeight: .infinity)
            }

        }
    }
}

struct PopupCalendarView: View {
    
    
    var days = ["Mon", "Tue", "Wed", "Thr", "Fri", "Sat", "Sun"]
    
    @State var displayingMonthAndYearStr = ""
    @ObservedObject var viewModel: CalendarViewModel
    
    var body: some View {
        VStack(spacing: 8) {
            HeaderView(currentMonth: $viewModel.currentSelectedMonthPopUp, selectedDate: $viewModel.selectedDate, viewModel: viewModel)
            HStack {
                ForEach(days, id: \.self) { day in
                    Text(day)
                        .frame(maxWidth: .infinity)
                }
            }
            Group {
                if self.viewModel.selection == -1 {
                    MonthVw(date: viewModel.prevMonthDate, viewModel: viewModel)
                } else if self.viewModel.selection == 0 {
                    MonthVw(date: viewModel.currentMonthDate, viewModel: viewModel)
                } else if self.viewModel.selection == 1 {
                    MonthVw(date: viewModel.nextMonthDate, viewModel: viewModel)
                }
            }
            .frame(height: 300)
            .animation(.default, value: self.viewModel.selection)
            .transition(AnyTransition.asymmetric(
                insertion:.move(edge: viewModel.isBack ? .leading : .trailing),
                removal: .move(edge: viewModel.isBack ? .trailing : .leading))
            )
            .simultaneousGesture(DragGesture(minimumDistance: 0, coordinateSpace: .local)
                .onEnded({ value in
                    if value.translation.width < 0 {
                        withAnimation {
                            viewModel.updatePopCalenderView(isPrevious: false)
                        }
                    }
                    if value.translation.width > 0 {
                        withAnimation {
                            viewModel.updatePopCalenderView(isPrevious: true)
                        }
                    }
                }))
        }
        .padding(.horizontal)
        .padding(.bottom, 8)
        .overlay(
            RoundedRectangle(cornerRadius: 16)
                .stroke(lineWidth: 1)
                .foregroundColor(Color.systemGray3)
                .shadow(color: .gray, radius: 6)
        )
        .background(Color.clear)
        .padding()
    }
    
}


struct MonthVw: View {
    
    @State var refresh: Bool = false
    var date: Date
    @ObservedObject var viewModel: CalendarViewModel
    var calendar = Calendar(identifier: .iso8601)
    
    var body: some View {
        MonthView(month: date) { date in
            VStack(spacing: 0) {
                Text("30")
                    .font(.footnote)
                    .hidden()
                    .padding(4)
                    .if(viewModel.selectedDate == date) { content in
                        content.overlay(
                            Circle()
                                .fill(.red)
                        )
                    }
                    .overlay(
                        Text(String(self.calendar.component(.day, from: date)))
                            .if(date == viewModel.selectedDate) { content in
                                content.foregroundColor(.white)
                            }
                    )
                    .simultaneousGesture(
                        TapGesture()
                            .onEnded { _ in
                                self.viewModel.selectedDate = date
                            }
                    )
                Spacer(minLength: 0)
                Circle()
                    .fill(.black)
                    .frame(width: 3, height: 3)
                Spacer(minLength: 0)
            }
        }
        .frame(height: 300)
        .fixedSize(horizontal: false, vertical: true)
    }
    
}



