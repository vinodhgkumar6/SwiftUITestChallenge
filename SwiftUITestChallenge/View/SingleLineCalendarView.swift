//
//  SingleLineCalendarView.swift
//  SwiftUITestChallenge
//
//  Created by vinodh kumar on 06/07/22.
//

import SwiftUI


struct SingleLineCalendarView: View {
    
    @State var monthString: String = "Not Set"
    
    let calendar = Calendar.current
    
    var currentDate = Date()
    
    @ObservedObject var viewModel: CalendarViewModel
    
    
    var body: some View {
        HStack(spacing: 8) {
            Button(action: {
                self.viewModel.setCurrentDateAsToday()
            }, label: {
                Text("Today")
                    .font(.caption)
                    .foregroundColor(.white)
                    .padding(4)
                    .background(Capsule()
                        .fill(.red))
            })
            if #available(iOS 14.0, *) {
                ScrollViewReader { scrollview in
                    ScrollView(.horizontal, showsIndicators: false) {
                        HStack(spacing: 16) {
                            ForEach($viewModel.dates, id: \.self) { day in
                                VStack {
                                    Text(getDayShort(date: day.wrappedValue))
                                        .font(.body)
                                        .fontWeight(.bold)
                                        .foregroundColor(day.wrappedValue == viewModel.selectedDate ? .red : .gray)
                                    Text("\(getDayNumber(date: day.wrappedValue)) \(getMonth(date: day.wrappedValue))")
                                        .font(.subheadline)
                                        .foregroundColor(day.wrappedValue == viewModel.selectedDate ? .red : .gray)
                                    Circle()
                                        .fill(day.wrappedValue == viewModel.selectedDate ? .red : .gray)
                                        .frame(width: 5, height: 5)
                                }
                                .onTapGesture {
                                    self.viewModel.selectedDate = day.wrappedValue
                                }
                            }
                        }
                    }
                    .coordinateSpace(name: "scroll")
                    .onReceive(viewModel.$selectedDate, perform: { date in
                        withAnimation {
                            scrollview.scrollTo(date, anchor: .center)
                        }
                    })
                }
            } else {
                //                ScrollViewReader { scrollview in
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack(spacing: 16) {
                        ForEach($viewModel.dates, id: \.self) { day in
                            VStack {
                                Text(getDayShort(date: day.wrappedValue))
                                    .font(.caption)
                                    .fontWeight(.bold)
                                    .foregroundColor(day.wrappedValue == viewModel.selectedDate ? .red : .gray)
                                Text("\(getDayNumber(date: day.wrappedValue)) \(getMonth(date: day.wrappedValue))")
                                    .font(.subheadline)
                                    .foregroundColor(day.wrappedValue == viewModel.selectedDate ? .red : .gray)
                                Circle()
                                    .fill(day.wrappedValue == viewModel.selectedDate ? .red : .gray)
                                    .frame(width: 5, height: 5)
                            }
                            .onTapGesture {
                                self.viewModel.selectedDate = day.wrappedValue
                            }
                        }
                    }
                }
                .coordinateSpace(name: "scroll")
                .onReceive(viewModel.$selectedDate, perform: { date in
                    withAnimation {
                        //                            scrollview.scrollTo(date, anchor: .center)
                    }
                })
            }
            //            }
            Button(action: {
                viewModel.showPopupCalendar = true
            }, label: {
                Image(systemName: "calendar")
                    .imageScale(.large)
                    .foregroundColor(.red)
            })
        }
    }
    
    func getMonth(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM"
        return dateFormatter.string(from: date)
    }
    
    func getDayShort(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E"
        return dateFormatter.string(from: date)
    }
    
    func getDayNumber(date: Date) -> Int {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: date)
        return components.day ?? 0
    }
}

struct SingleLineCalendarView_Previews: PreviewProvider {
    static var previews: some View {
        SingleLineCalendarView(viewModel: CalendarViewModel())
    }
}
