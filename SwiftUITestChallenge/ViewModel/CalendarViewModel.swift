//
//  CalendarViewModel.swift
//  SwiftUITestChallenge
//
//  Created by vinodh kumar on 06/07/22.
//

import Foundation

class CalendarViewModel: ObservableObject {
    
    @Published var selectedDate: Date = Date().getCurrentDate()
    @Published var dates = [Date]()
    @Published var showPopupCalendar = false
    @Published var currentSelectedMonthPopUp = 0
    
//    @Published var months = [Date]()
    
    @Published var prevMonthDate: Date = Date().getCurrentDate()
    @Published var nextMonthDate: Date = Date().getCurrentDate()
    @Published var currentMonthDate: Date = Date().getCurrentDate()
    var isBack: Bool = false
    @Published var selection = 0
    

    init() {
        self.dates = self.getWeeks(date: selectedDate)
        self.prevMonthDate = getCurrentMonth(month: -1)
        self.nextMonthDate = getCurrentMonth(month: 1)
       
    }

    func setCurrentDateAsToday() {
        
        self.selectedDate = Date().getCurrentDate()
        self.dates = self.getWeeks(date: selectedDate)
        if currentSelectedMonthPopUp > 0 {
            self.currentSelectedMonthPopUp = 0
            self.updatePopCalenderView(isPrevious: true, isToday: true)
        } else if currentSelectedMonthPopUp < 0 {
            self.currentSelectedMonthPopUp = 0
            self.updatePopCalenderView(isPrevious: false, isToday: true)
        }
        
    }
    
    func getWeeks(date: Date) -> [Date] {
        let calendar = Calendar(identifier: .iso8601)
        let dayOfWeek = calendar.component(.weekday, from: date)
        let daysMonth = (-1000 ..< 1000)
            .compactMap { calendar.date(byAdding: .day, value: $0 - dayOfWeek, to: date) }
        return daysMonth
    }
    
//    static func getMonths() -> [CustomDate] {
//        let range = -50...50
//        let calendar = Calendar(identifier: .iso8601)
//        let months = range.map { idx in
//            return CustomDate(id: idx, date: calendar.date(byAdding: .month, value: idx, to: Date())!)
//        }
//        return months
//    }
    
    func getCurrentMonth(month: Int) -> Date {
        let calendar = Calendar(identifier: .iso8601)
        return calendar.date(byAdding: .month, value: month, to: Date())!
    }
    
    func updatePopCalenderView(isPrevious: Bool = false, isToday: Bool = false) {
        isBack = isPrevious
        self.selection = isPrevious ? -1 : 1
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.selection = 0
            if !isToday {
                if isPrevious {
                    self.currentSelectedMonthPopUp -= 1
                } else {
                    self.currentSelectedMonthPopUp += 1
                }
            }

            self.currentMonthDate = self.getCurrentMonth(month: self.currentSelectedMonthPopUp)
            self.prevMonthDate = self.getCurrentMonth(month: -1)
            self.nextMonthDate = self.getCurrentMonth(month: 1)
        })
    }
}


